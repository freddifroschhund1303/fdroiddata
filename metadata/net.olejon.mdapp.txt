Categories:Office
License:GPLv3
Web Site:http://olejon.github.io/mdapp/
Source Code:https://github.com/olejon/mdapp
Issue Tracker:https://github.com/olejon/mdapp/issues

Auto Name:LegeAppen
Summary:Toolkit for medical personnel
Description:
Helpful utilities for medical personnel and health care workers in Norway. The
apps is in Norwegian.

Features:

* Felleskatalogen, with all medications and substances
* Diseases and treatments, with search in many recognized sources
* Emergency room handbook
* National guidelines
* Interaction analysis
* Poisonings information
* ATC-registry
* ICD-10
* Overview over all pharmaceutical companies with medications on the market
* Overview over all pharmacies, with contact information, opening times and map location
* Clinical trials
* Notifications from The Norwegian Medicines Agency
* Search after text in medication texts
* Print medication texts and websites you open in the app
* Scan barcodes on medication boxes
* Widget for your home screen with your favorite medications
* ...

Vital parts of the app linked to Felleskatalogen work without Internet access.
The rest requires Internet access. The app is optimized for the lowest possible
data usage.

Required Permissions:

* In-app purchases: To be able to donate money to the developer
* Location: To find your position in relation to a pharmacy
* Photos/Media/Files: To store Felleskatalogen
* Camera: To scan barcodes
* Wi-Fi connection information: To check for WiFi connectivity

Disclaimer:
LegeAppen is developed by a private person, and even though the content in
the app is fetched from recognized sources, the developer can not the be
held responsible for decisions taken based on information found in this app.
.

Repo Type:git
#Repo:https://github.com/olejon/mdapp
Repo:https://github.com/olejon/mdapp-fdroid

Build:1.1,110
    commit=45f68902deb7f8d830a930d3713c1866a52caa23
    subdir=mdapp/app
    gradle=yes
    prebuild=pushd src/main/assets && \
        wget -c http://www.olejon.net/code/mdapp/api/1/felleskatalogen/db/felleskatalogen.full.db.zip -O felleskatalogen.db.zip && \
        popd

Build:1.2,120
    commit=5090c3eb5ac0ee59bd24bd67a08768d30e194bb7
    subdir=mdapp/app
    gradle=yes
    prebuild=pushd src/main/assets && \
        wget -c http://www.olejon.net/code/mdapp/api/1/felleskatalogen/db/felleskatalogen.full.db.zip -O felleskatalogen.db.zip && \
        popd

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.2
Current Version Code:120

