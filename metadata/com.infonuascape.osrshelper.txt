AntiFeatures:NonFreeNet
Categories:Games
License:GPLv3
Web Site:https://github.com/ldionmarcil/OSRSHelper/blob/HEAD/README.md
Source Code:https://github.com/ldionmarcil/OSRSHelper
Issue Tracker:https://github.com/ldionmarcil/OSRSHelper/issues

Auto Name:OSRS Helper
Summary:View your RuneScape stats
Description:
View stats of your [http://oldschool.runescape.com/ OldSchool Runescape]
character.
.

Repo Type:git
Repo:https://github.com/ldionmarcil/OSRSHelper

Build:1.1.1,3
    commit=7c8b3ba8f400e280e36fec17779a295e94a70a5d
    target=android-19

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.1.1
Current Version Code:3

